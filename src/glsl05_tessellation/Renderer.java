package glsl05_tessellation;

import com.jogamp.opengl.DebugGL2;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

import oglutils.OGLBuffers;
import oglutils.OGLTextRenderer;
import oglutils.OGLUtils;
import oglutils.ShaderUtils;

import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

/**
* Ukazka pro praci s shadery v GLSL:
* upraveno pro JOGL 2.3.0 a vyssi
* 
* @author PGRF FIM UHK
* @version 2.0
* @since   2016-09-26 
*/
public class Renderer implements GLEventListener, MouseListener,
		MouseMotionListener, KeyListener {

	int width, height;

	OGLBuffers buffers2;
	OGLTextRenderer textRenderer = new OGLTextRenderer();
	
	int shaderProgram, locTime;

	float time = 1;
	
	int demoType = 0;
	boolean demoTypeChanged = true;
			
	public void init(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();
	
		//debug
		//glDrawable.setGL(new DebugGL2(gl));
		//gl = glDrawable.getGL().getGL2();
		
		OGLUtils.printOGLparameters(gl);
		OGLUtils.shaderCheck(gl);
		
		if (OGLUtils.getVersion(gl) >= 400){
			int[] maxPatchVertices = new int[1];
			gl.glGetIntegerv(GL4.GL_MAX_PATCH_VERTICES,maxPatchVertices, 0);
			System.out.println("Max supported patch vertices "	+ maxPatchVertices[0]);
		}
	
		createBuffers(gl);
		
	}
	
	void createBuffers(GL2 gl) {
		/*float[] vertexBufferData = {
			-1, -1, 	0.7f, 0, 0, 
			 1,  0,		0, 0.7f, 0,
			 0,  1,		0, 0, 0.7f 
		};
		OGLBuffers.Attrib[] attributes = {
				new OGLBuffers.Attrib("inPosition", 2),
				new OGLBuffers.Attrib("inColor", 3)
		};*/
		
		//int[] indexBufferData = { 0,1,2,3, 2,3,4,5, 4,5,6,7, 6,7,0,1};
		int[] indexBufferData = { 0,1,2};
		
		float[] vertexBufferDataPos = {
			-0.8f, -0.9f, 
			-0.8f, 0.6F,
			0.6f, 0.8f, 
			-0.6f, -0.8f, 
			0.8f, -0.8f, 
			0.9f, 0.9f, 
			0.6f, 0.3f, 
			0.6f, -1.0f 
				};
		
		float[] vertexBufferDataCol = {
			0, 1, 0, 
			1, 0, 0,
			1, 1, 0,
			0, 0, 1, 
			0, 0, 1, 
			1, 1, 1, 
			1, 1, 0, 
			1, 0, 1 
		};
		OGLBuffers.Attrib[] attributesPos = {
				new OGLBuffers.Attrib("inPosition", 2),
		};
		OGLBuffers.Attrib[] attributesCol = {
				new OGLBuffers.Attrib("inColor", 3)
		};
		buffers2 = new OGLBuffers(gl, vertexBufferDataPos, attributesPos,
				indexBufferData);
		buffers2.addVertexBuffer(vertexBufferDataCol, attributesCol);

	}

	private int init(GL2 gl, int demoType){
		String extensions = OGLUtils.getExtensions(gl);
		int newShaderProgram = 0;
		switch (demoType){
		case 0: //pouze VS a FS
			System.out.println("Pipeline: VS + FS");
			if (extensions.indexOf("GL_ARB_enhanced_layouts") == -1)
				newShaderProgram = ShaderUtils.loadProgram(gl, "/shader/glsl05/tessel_OlderSM_WithoutGS",
						"/shader/glsl05/tessel_OlderSM_WithoutGS",
						null,null,null,null); 
			else
				newShaderProgram = ShaderUtils.loadProgram(gl, "/shader/glsl05/tessel",
						"/shader/glsl05/tessel",
						null,null,null,null); 
			break;
		case 1: // pouze VS, FS a GS
			System.out.println("Pipeline: VS + GS + FS");
			if (OGLUtils.getVersion(gl) >= 300) {
				if (extensions.indexOf("GL_ARB_enhanced_layouts") == -1)
					newShaderProgram = ShaderUtils.loadProgram(gl, "/shader/glsl05/tessel_OlderSM_OnlyGS",
							"/shader/glsl05/tessel_OlderSM_OnlyGS", "/shader/glsl05/tessel_OlderSM_OnlyGS", null, null,
							null); 
				else 
					newShaderProgram = ShaderUtils.loadProgram(gl, "/shader/glsl05/tessel", "/shader/glsl05/tessel",
							"/shader/glsl05/tessel", null, null, null); 
			} else
				System.out.println("Geometry shader is not supported");
			break;
		case 2: //pouze VS, FS a tess
			System.out.println("Pipeline: VS + tess + FS");
			if (OGLUtils.getVersion(gl) >= 400) {
				newShaderProgram = ShaderUtils.loadProgram(gl, "/shader/glsl05/tessel",
						"/shader/glsl05/tessel",
						null,
						"/shader/glsl05/tessel",
						"/shader/glsl05/tessel",
						null); 
				}	
			else
				System.out.println("Tesselation is not supported");
			break;
		default: //pouze VS, FS, GS a tess
			System.out.println("Pipeline: VS + tess + GS + FS");
			if (OGLUtils.getVersion(gl) >= 400) {
				newShaderProgram = ShaderUtils.loadProgram(gl, "/shader/glsl05/tessel");
			}	
			else
				System.out.println("Tesselation is not supported");
		}
		
		return newShaderProgram;	
		
	}

	public void display(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();
		
		if (demoTypeChanged) {
			int oldShaderProgram = shaderProgram;
			shaderProgram = init(gl,demoType);
			if (shaderProgram>0) {
				gl.glDeleteProgram(oldShaderProgram);
			} else {
				shaderProgram = oldShaderProgram;
			}
			locTime = gl.glGetUniformLocation(shaderProgram, "time");
			demoTypeChanged = false;
		}
		
		
		gl.glClearColor(0.2f, 0.1f, 0.1f, 1.0f);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

		time *= 1.01;
		time =  time % 100;
		
		//System.out.println(time);
		
		gl.glUseProgram(shaderProgram); 
		gl.glUniform1f(locTime, time); 
		
		gl.glPolygonMode(GL2.GL_FRONT, // GL2.GL_FRONT_AND_BACK,GL2.GL_FRONT,GL2.GL_BACK
				GL2.GL_LINE); // GL2.GL_LINE,GL2.GL_POINT,GL2.GL_FILL
		gl.glPolygonMode(GL2.GL_BACK, // GL2.GL_FRONT_AND_BACK,GL2.GL_FRONT,GL2.GL_BACK
				GL2.GL_LINE); // GL2.GL_LINE,GL2.GL_POINT,GL2.GL_FILL

		// vykresleni
		switch (demoType){
		case 1: //points VS+GS+FS
			if (OGLUtils.getVersion(gl) >= 300){
				buffers2.draw(GL2.GL_TRIANGLES, shaderProgram);
			}	
			break;		
		case 2: //tessellation VS+TCS+TES+FS
		case 3: //points VS+TCS+TES+GS+FS
			if (OGLUtils.getVersion(gl) >= 400){
				gl.getGL3().glPatchParameteri(GL4.GL_PATCH_VERTICES, 3);
				buffers2.draw(GL4.GL_PATCHES, shaderProgram);
			}
			break;		
		default: //triangle VS+FS
			buffers2.draw(GL2.GL_TRIANGLES, shaderProgram); 
			break;		
		}
		//buffers2.draw(GL3.GL_LINES_ADJACENCY, shaderProgram);
		//buffers2.draw(GL2.GL_LINE_STRIP, shaderProgram);
		
		String text = new String(this.getClass().getName() + ": [m]ode " + time);
		
		textRenderer.drawStr2D(glDrawable, 3, height-20, text);
		textRenderer.drawStr2D(glDrawable, width-90, 3, " (c) PGRF UHK");
	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		this.width = width;
		this.height = height;
	}

	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
			boolean deviceChanged) {
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseDragged(MouseEvent e) {
	}

	public void mouseMoved(MouseEvent e) {
	}

	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_M:
			demoType = (demoType+1) % 4;
			demoTypeChanged = true;
			break;
		}
}

	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

	public void dispose(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();
		gl.glDeleteProgram(shaderProgram);
	}
}