package cviceni02;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

import oglutils.OGLBuffers;
import oglutils.OGLTextRenderer;
import oglutils.OGLTexture2D;
import oglutils.OGLUtils;
import oglutils.ShaderUtils;
import oglutils.ToFloatArray;
import transforms.Camera;
import transforms.Mat4;
import transforms.Mat4PerspRH;
import transforms.Vec3D;

/**
 * GLSL sample:<br/>
 * Read and compile shader from files "/shader/glsl01/start.*" using ShaderUtils
 * class in oglutils package (older GLSL syntax can be seen in
 * "/shader/glsl01/startForOlderGLSL")<br/>
 * Manage (create, bind, draw) vertex and index buffers using OGLBuffers class
 * in oglutils package<br/>
 * Requires JOGL 2.3.0 or newer
 * 
 * @author PGRF FIM UHK
 * @version 2.0
 * @since 2015-09-05
 */
public class Renderer implements GLEventListener, MouseListener,
		MouseMotionListener, KeyListener {

	int width, height, ox, oy;
	
	GL2 gl;
	GLAutoDrawable glDrawable;
	Camera cam = new Camera();

	Grid grid;
	OGLBuffers oglBuffers;
	OGLTextRenderer textRenderer = new OGLTextRenderer();
	
	int shaderProgram, locTime,locMat,locMatView,locMatProjection; // locMat(pohledovÃ¡ * projekÄnÃ­),locMatView - view(pohledovÃ¡) matice
	
	Mat4 proj; // created in reshape()

	OGLTexture2D texture,texturen; // textura
	OGLTexture2D.Viewer textureViewer;
	
	float time = 0;

	public void init(GLAutoDrawable drawable) {
		glDrawable = drawable;
		gl = glDrawable.getGL().getGL2();
		
		grid = new Grid(gl);
		oglBuffers = grid.createOGLBuffersTriangles(50, 50);
		
		//oglBuffers = grid.createOGLBuffersTriangleFans(3);
		
		
		// shader files are in /res/shader/ directory
		// res directory must be set as a source directory of the project
		// e.g. in Eclipse via main menu Project/Properties/Java Build Path/Source
		
		// fce klobouku s osvetlením per vertex
		//shaderProgram = ShaderUtils.loadProgram(gl, "/shader/cviceni02du/osvetleni_per_vertex_klobouk");
		// fce koule s osvetlením per vertex
		//shaderProgram = ShaderUtils.loadProgram(gl, "/shader/cviceni02du/osvetleni_per_vertex_koule");
		shaderProgram = ShaderUtils.loadProgram(gl, "/shader/cviceni03/osvetleni");
		
		
		/* V OGLTexture2D místo clam_to_edge dát GL_REPEAT pro oba směry S i T				
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_CLAMP_TO_EDGE);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_CLAMP_TO_EDGE);
		*/

// základni vykreslení gridu s kamerou pro debug
		//shaderProgram = ShaderUtils.loadProgram(gl, "/shader/cviceni01/camera");
		
		// vykreslení gridu pomocí fanů
                //shaderProgram = ShaderUtils.loadProgram(gl, "/shader/cviceni03du_fan/camera");
		//shaderProgram = ShaderUtils.loadProgram(gl, "/shader/cviceni05/textura");
		
			//older GLSL version "/shader/glsl01/startForOlderGLSL"
		
		locMat = gl.glGetUniformLocation(shaderProgram, "mat");
		locMatView = gl.glGetUniformLocation(shaderProgram,"matView");
		locMatProjection = gl.glGetUniformLocation(shaderProgram,"matProjection");
		
		//umisteni souboru je v adresari /res/textures/...
				texture = new OGLTexture2D(gl, "/textures/bricks.jpg");
				texturen = new OGLTexture2D(gl, "/textures/bricksn.png"); // normálová textura
				textureViewer = new OGLTexture2D.Viewer(gl);
				
		cam = cam.withPosition(new Vec3D(5, 5, 2.5))
				.withAzimuth(Math.PI * 1.25)
				.withZenith(Math.PI * -0.125);
		
		gl.glEnable(GL2.GL_DEPTH_TEST);

	}
	
	public void display(GLAutoDrawable glDrawable) {
		
		gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
		
		// z buffer
		gl.glEnable(GL2.GL_DEPTH_TEST);
		
		// set the current shader to be used, could have been done only once (in
		// init) in this sample (only one shader used)
		gl.glUseProgram(shaderProgram);  // correct shader must be set before this
		

		texture.bind(shaderProgram, "texture", 0);
		
		texture.bind(shaderProgram, "texturen", 1); // normálová textura na druhý slot
		// pÅ™idÃ¡nÃ­ view * projection matice pro vÃ½poÄet pozice objektu
		gl.glUniformMatrix4fv(locMat, 1, false,
				ToFloatArray.convert(cam.getViewMatrix().mul(proj)), 0);
		
		// přidání pohledové matice pro výpočet per vertex osvětlení
		gl.glUniformMatrix4fv(locMatView, 1, false, ToFloatArray.convert(cam.getViewMatrix()),0);
		
		gl.glUniformMatrix4fv(locMatProjection, 1, false, ToFloatArray.convert(proj),0);
		// bind and draw
		/*gl.glPolygonMode(GL2.GL_FRONT, GL2.GL_LINES);
		gl.glPolygonMode(GL2.GL_BACK, GL2.GL_LINES);*/
		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_LINE); // DB vykreslení pouze linií trojuhelníkù

                gl.glPolygonMode(GL2.GL_FRONT, GL2.GL_FILL);

		textureViewer.view(texture, -1, -1, 0.5);
		textureViewer.view(texturen, -1, -0.5, 0.5);
		
		
		oglBuffers.draw(GL2.GL_TRIANGLES, shaderProgram);
		//oglBuffers.draw(GL2.GL_TRIANGLE_FAN, shaderProgram);


	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		this.width = width;
		this.height = height;
		proj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 0.01, 1000.0);
	}

	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
			boolean deviceChanged) {
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
		ox = e.getX();
		oy = e.getY();
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseDragged(MouseEvent e) {
		cam = cam.addAzimuth((double) Math.PI * (ox - e.getX()) / width)
				.addZenith((double) Math.PI * (e.getY() - oy) / width);
		ox = e.getX();
		oy = e.getY();
	}

	public void mouseMoved(MouseEvent e) {
	}

	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_W:
			cam = cam.forward(1);
			break;
		case KeyEvent.VK_D:
			cam = cam.right(1);
			break;
		case KeyEvent.VK_S:
			cam = cam.backward(1);
			break;
		case KeyEvent.VK_A:
			cam = cam.left(1);
			break;
		case KeyEvent.VK_CONTROL:
			cam = cam.down(1);
			break;
		case KeyEvent.VK_SHIFT:
			cam = cam.up(1);
			break;
		case KeyEvent.VK_SPACE:
			cam = cam.withFirstPerson(!cam.getFirstPerson());
			break;
		case KeyEvent.VK_R:
			cam = cam.mulRadius(0.9f);
			break;
		case KeyEvent.VK_F:
			cam = cam.mulRadius(1.1f);
			break;
		}
	}

	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

	public void dispose(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();
		gl.glDeleteProgram(shaderProgram);
	}

}