package projekt;

import com.jogamp.opengl.GL2;

import oglutils.OGLBuffers;
import oglutils.ToFloatArray;
import oglutils.ToIntArray;

public class Grid {
	private GL2 gl;
	
	public Grid(GL2 gl){
		this.gl = gl;
	}

	public OGLBuffers createOGLBuffersTriangles(int m, int n){
		
		int vertices = m*n;
		int triangles = (m-1) * (n-1) * 2;
		int pocitadlo = 0;
		
		
		
		float [] vb = new float[2*vertices];
		int[] ib = new int [3*triangles];
		
		for (int i = 0; i< m; i++){
			for (int j = 0; j < n; j++){
				vb[(i*m+j)*2] =  (float) i/(m-1);
				vb[(i*m+j)*2+1] =  (float) j/(n-1);
			}
		}
		pocitadlo = 0;
		for (int k = 0; k < m-1; k++){
			for (int j = 0; j < n-1; j++){
				int i = k*m + j;
				
				ib[pocitadlo] = i;
				ib[pocitadlo+1] = i + n ;
				ib[pocitadlo+2] = i + n + 1;
				ib[pocitadlo+3] = i;
				ib[pocitadlo+4] = i + n + 1;
				ib[pocitadlo+5] = i + 1;
				
				pocitadlo = pocitadlo + 6; 		 
		}
		}
		
		// kontrola
		//this.printBuffers(vb, ib);
		
		OGLBuffers.Attrib[] attributes = {
				new OGLBuffers.Attrib("inPosition", 2)};
				
		return new OGLBuffers(gl, ToFloatArray.convert(vb), attributes, ib);
	}
	
	public OGLBuffers createOGLBuffersQuads(int m, int n){
		
		
	return null;
	}
	
// algoritmus je správně dle přednášky vypisuje jak má.. ale glsl to implementuje zdá se jinak
	// todo bude třeba přes begin a endy vykreslovat
	// viz http://opengl.czweb.org/ch06/149-153.html
	public OGLBuffers createOGLBuffersTriangleFans(int m){
		
		int vertices = m;
		// TODO kontrola na vstupní param.. liché �?íslo > 3
		int fansPerRow =(int) (double)(vertices-1)/2; 
		int fans = (int) Math.pow( (double) fansPerRow, 2); 
		int pocitadlo = 0;
		
		// DB TMP DEBUG
		System.out.println("FANS per ROW = "+fansPerRow);
		System.out.println("FANS = "+fans);
		
		
		
		float [] vb = new float[2*vertices*vertices];
		
		//int[] ib = new int [(int) Math.pow( (double) (5/2)*(vertices-1),2)]; // krása...:D
		int[] ib = new int [fans*9]; // krása...:D

		
		// vertex Buffer
		for (int i = 0; i< vertices; i++){
			for (int j = 0; j < vertices; j++){
				vb[(i*vertices+j)*2] =  (float) i/vertices;
				vb[(i*vertices+j)*2+1] =  (float) j/vertices;
			}
		}
		
		pocitadlo = 0;
		// index buffer
		for (int i = 0; i < fansPerRow; i++){
			for(int j =0; j<fansPerRow;j++){
				
				ib[pocitadlo++]=(2*(vertices)*i+(vertices-1))+(2*j+2); //4
				
				ib[pocitadlo++]=(2*(vertices)*i) + (2*j);  //0
				ib[pocitadlo++]=(2*(vertices)*i) + (2*j+1); //1
				ib[pocitadlo++]=(2*(vertices)*i) + (2*j+2); //2
				
				
				ib[pocitadlo++]=(2*(vertices)*i+(vertices-1))+(2*j+3); //5
				
				ib[pocitadlo++]=(2*(vertices)*i)+2*(vertices)- 1 +(2*j+3); //8
				ib[pocitadlo++]=(2*(vertices)*i)+2*(vertices) -1 +(2*j+2);//7
				ib[pocitadlo++]=(2*(vertices)*i)+2*(vertices) -1 +(2*j+1);//6
				
				ib[pocitadlo++]=(2*(vertices)*i+(vertices-1))+(2*j+1); //3
				
				 
				
			}
		}
		
		
/*		
		int x = fans*9;
		pocitadlo = 0;
		int row = 0;
		for (int i = 0; i< x;i++){
			ib[pocitadlo++] = 2* i * (vertices-1) ;  
		}*/
		
		// kontrola
		//this.printBuffers(vb, ib);
		
		OGLBuffers.Attrib[] attributes = {
				new OGLBuffers.Attrib("inPosition", 2)};
				
		return new OGLBuffers(gl, ToFloatArray.convert(vb), attributes, ib);
	}
	
	// vytiskne vertex a index buffer
	public static void printBuffers(float [] vertBuffer,int[] indexBuffer){
		System.out.println(" VERTEX BUFFER ");
		for (int i = 0; i < vertBuffer.length; i++) {
			System.out.print(vertBuffer[i]+", ");
		}
		/*System.out.println(" INDEX BUFFER ");
		for (int i = 0; i < indexBuffer.length; i++) {
			System.out.println(indexBuffer[i]+ " ");
			
		}*/
		
	}
}
