package projekt;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

import oglutils.OGLBuffers;
import oglutils.OGLTextRenderer;
import oglutils.OGLTexture2D;
import oglutils.OGLUtils;
import oglutils.ShaderUtils;
import oglutils.ToFloatArray;
import transforms.Camera;
import transforms.Mat4;
import transforms.Mat4PerspRH;
import transforms.Vec3D;

/**
 * GLSL sample:<br/>
 * Read and compile shader from files "/shader/glsl01/start.*" using ShaderUtils
 * class in oglutils package (older GLSL syntax can be seen in
 * "/shader/glsl01/startForOlderGLSL")<br/>
 * Manage (create, bind, draw) vertex and index buffers using OGLBuffers class
 * in oglutils package<br/>
 * Requires JOGL 2.3.0 or newer
 * 
 * @author PGRF FIM UHK
 * @version 2.0
 * @since 2015-09-05
 */
public class Renderer implements GLEventListener, MouseListener,
		MouseMotionListener, KeyListener {

	int width, height, ox, oy;
	
	GL2 gl;
	GLAutoDrawable glDrawable;
	Camera cam = new Camera();

	Grid grid;
	OGLBuffers oglBuffers;
	OGLTextRenderer textRenderer = new OGLTextRenderer();
	
	int shaderProgram0,locTime0,locClosingIncrement0,locFunctionSwitcher0,locMat0,locMatView0,locMatProjection0; 
	// osvetleni a barvy
	int locLightPosition0,locBaseColor0,locAmbientColor0,locDiffuseColor0,locSpecularColor0;
	// reflektor a utlum
	int locSpotCutOff0,locConstantAtt0,locLinearAtt0,locQuadraticAtt0, locCBumpSize0;
	
	int shaderProgram1,locTime1,locClosingIncrement1,locFunctionSwitcher1,locMat1,locMatView1,locMatProjection1; 
	// osvetleni a barvy
	int locLightPosition1,locBaseColor1,locAmbientColor1,locDiffuseColor1,locSpecularColor1;
	// reflektor a utlum
	int locSpotCutOff1,locConstantAtt1,locLinearAtt1,locQuadraticAtt1, locCBumpSize1;
	
	int shaderProgram2,locTime2,locClosingIncrement2,locFunctionSwitcher2,locMat2,locMatView2,locMatProjection2; 
	// osvetleni a barvy
	int locLightPosition2,locBaseColor2,locAmbientColor2,locDiffuseColor2,locSpecularColor2;
	// reflektor a utlum
	int locSpotCutOff2,locConstantAtt2,locLinearAtt2,locQuadraticAtt2, locCBumpSize2;
	
	int shaderProgram3, locTime3,locClosingIncrement3,locFunctionSwitcher3,locMat3,locMatView3,locMatProjection3; 
	// osvetleni a barvy
	int locLightPosition3,locBaseColor3,locAmbientColor3,locDiffuseColor3,locSpecularColor3;
	// reflektor a utlum
	int locSpotCutOff3,locConstantAtt3,locLinearAtt3,locQuadraticAtt3, locCBumpSize3;
	
	Mat4 proj; // created in reshape()

	OGLTexture2D texture,texturen,textureh; // textura
	OGLTexture2D.Viewer textureViewer;
	
	// lava function
	float time = 1.0f;
	boolean increasing;
	static float TIME_LOWER_CAP = 1.0f;
	static float TIME_UPPER_CAP = 100f;
	
	// sfere closing function
	float closing_increment = 0.1f;
	boolean closing;
	static float CLOSING_LOWER_CAP = 0.1f;
	static float CLOSING_UPPER_CAP = 2.0f;
	
	// switching shader with different programs
	int shaderSwitcher;
	// function switcher from gui
	int functionSwitcher;
	// modes
	int glFront = 0;
	int glBack = 0;
	
	// svetlo a barvy
	protected float lightPositionX,lightPositionY,lightPositionZ,
	baseColorR,baseColorG,baseColorB,baseColorA,
	ambientR,ambientG,ambientB,ambientA,
	diffuseR,diffuseG,diffuseB,diffuseA,
	specularR,specularG,specularB,specularA;
	
	// relfektor a utlum
	protected float spotCutOff,constantAtt,linearAtt,quadraticAtt,cBumpSizeX,cBumpSizeY; 
	
	
		
	
	public void init(GLAutoDrawable drawable) {
		glDrawable = drawable;
		gl = glDrawable.getGL().getGL2();
		
		grid = new Grid(gl);
		oglBuffers = grid.createOGLBuffersTriangles(100,100);
		
		//oglBuffers = grid.createOGLBuffersTriangleFans(3);
		
		
		// shader files are in /res/shader/ directory
		// res directory must be set as a source directory of the project
		// e.g. in Eclipse via main menu Project/Properties/Java Build Path/Source
		
		// fce klobouku s osvetlením per vertex
		//shaderProgram = ShaderUtils.loadProgram(gl, "/shader/cviceni02du/osvetleni_per_vertex_klobouk");
		// fce koule s osvetlením per vertex
		//shaderProgram = ShaderUtils.loadProgram(gl, "/shader/cviceni02du/osvetleni_per_vertex_koule");
		//shaderProgram = ShaderUtils.loadProgram(gl, "/shader/cviceni03/osvetleni");
		
		
		/* V OGLTexture2D místo clam_to_edge dát GL_REPEAT pro oba směry S i T				
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_CLAMP_TO_EDGE);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_CLAMP_TO_EDGE);
		*/

// základni vykreslení gridu s kamerou pro debug
		//shaderProgram = ShaderUtils.loadProgram(gl, "/shader/cviceni01/camera");
		
		// vykreslení gridu pomocí fanů
                //shaderProgram = ShaderUtils.loadProgram(gl, "/shader/cviceni03du_fan/camera");
		//shaderProgram = ShaderUtils.loadProgram(gl, "/shader/cviceni05/textura");
		
			//older GLSL version "/shader/glsl01/startForOlderGLSL"
		
		// projektov� shadery
		//shaderProgram = ShaderUtils.loadProgram(gl, "/shader/projekt/osvetleni_per_vertex");
		//shaderProgram = ShaderUtils.loadProgram(gl, "/shader/projekt/osvetleni_per_pixel");
		//shaderProgram = ShaderUtils.loadProgram(gl, "/shader/projekt/osvetleni_per_pixel_bp");
		//shaderProgram = ShaderUtils.loadProgram(gl, "/shader/projekt/osvetleni_n_textura");
		//shaderProgram = ShaderUtils.loadProgram(gl, "/shader/projekt/osvetleni_n_textura_normal_mapping");
		//shaderProgram = ShaderUtils.loadProgram(gl, "/shader/projekt/osvetleni_n_textura_normal_mapping");
		//shaderProgram = ShaderUtils.loadProgram(gl, "/shader/projekt/osvetleni_n_textura_parallax_mapping");
		
		shaderProgram0 = ShaderUtils.loadProgram(gl, "/shader/projekt/osvetleni_per_vertex");
		shaderProgram1 = ShaderUtils.loadProgram(gl, "/shader/projekt/osvetleni_per_pixel_bp");
		shaderProgram2 = ShaderUtils.loadProgram(gl, "/shader/projekt/osvetleni_n_textura_normal_mapping");
		shaderProgram3 = ShaderUtils.loadProgram(gl, "/shader/projekt/osvetleni_n_textura_parallax_mapping");
		
		
		addVariableToShaderPrograms();
		
		//umisteni souboru je v adresari /res/textures/...
				texture = new OGLTexture2D(gl, "/textures/bricks.jpg");
				texturen = new OGLTexture2D(gl, "/textures/bricksn.png"); // normálová textura
				textureh = new OGLTexture2D(gl, "/textures/bricksh.png");
				textureViewer = new OGLTexture2D.Viewer(gl);
				
		cam = cam.withPosition(new Vec3D(5, 5, 2.5))
				.withAzimuth(Math.PI * 1.25)
				.withZenith(Math.PI * -0.125);
		
		gl.glEnable(GL2.GL_DEPTH_TEST);
		System.out.println("program "+shaderSwitcher);
	}
	
	public void display(GLAutoDrawable glDrawable) {
		
		gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
		
		// z buffer
		gl.glEnable(GL2.GL_DEPTH_TEST);
		
		/*if (shaderSwitcher == 1 ) gl.glUseProgram(shaderProgram1);
		else if (shaderSwitcher == 2 ) gl.glUseProgram(shaderProgram2);
		else if (shaderSwitcher == 3 ) gl.glUseProgram(shaderProgram3);
		else  gl.glUseProgram(shaderProgram0);*/
		
		
		// set the current shader to be used, could have been done only once (in
		// init) in this sample (only one shader used)
		if (shaderSwitcher == 0) gl.glUseProgram(shaderProgram0);
		else if (shaderSwitcher == 1) gl.glUseProgram(shaderProgram1);
		else if (shaderSwitcher == 2) gl.glUseProgram(shaderProgram2);
		else if (shaderSwitcher == 3) gl.glUseProgram(shaderProgram3);
		//gl.glUseProgram(shaderProgram);  // correct shader must be set before this
		
		
		// lava function
		if (increasing){
			time += time/(float)JOGLApp.FPS;
		} else {
			time -= time/(float)JOGLApp.FPS;
		}
		
		// sfere closing function
		if (closing){
			closing_increment-=closing_increment/(float)JOGLApp.FPS;
		}else {
			closing_increment+=closing_increment/(float)JOGLApp.FPS;
		}
		
		// System.out.println("time is "+time); // DEBUG
		// lava function
		if (time > Renderer.TIME_UPPER_CAP) increasing = false;
		if (time < Renderer.TIME_LOWER_CAP) increasing = true;
		
		// System.out.println("sfere"+closing_increment); // DEBUG
		// sfere function
		if (closing_increment > Renderer.CLOSING_UPPER_CAP) closing = true;
		if (closing_increment < Renderer.CLOSING_LOWER_CAP) closing = false;
		
		// predani promennych do shaderu
		this.addVariableToShaderDisplay();
		
		texture.bind(shaderProgram2, "texture", 1);
		texturen.bind(shaderProgram2, "texturen", 2); // normálová textura na druhý slot
		textureh.bind(shaderProgram2, "textureh", 3); // vy�kova textura
		texture.bind(shaderProgram3, "texture", 1);
		texturen.bind(shaderProgram3, "texturen", 2); // normálová textura na druhý slot
		textureh.bind(shaderProgram3, "textureh", 3); // vy�kova textura
		
		
		
	
		
		// bind and draw
		/*
		gl.glPolygonMode(GL2.GL_BACK, GL2.GL_LINES);*/
		//gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_LINE); // DB vykreslení pouze linií trojuhelníkù
		
		if (glFront == 1 ) gl.glPolygonMode(GL2.GL_FRONT, GL2.GL_LINE);
		else gl.glPolygonMode(GL2.GL_FRONT, GL2.GL_FILL);
		
		if (glBack == 1) gl.glPolygonMode(GL2.GL_BACK, GL2.GL_LINE);
		else gl.glPolygonMode(GL2.GL_BACK, GL2.GL_FILL);
		
		textureViewer.view(texture, -1, -1, 0.5);
		textureViewer.view(texturen, -1, -0.5, 0.5);
		textureViewer.view(textureh, -1, 0, 0.5);
		
		
		if (shaderSwitcher == 0) oglBuffers.draw(GL2.GL_TRIANGLES, shaderProgram0);
		else if (shaderSwitcher == 1) oglBuffers.draw(GL2.GL_TRIANGLES, shaderProgram1);
		else if (shaderSwitcher == 2) oglBuffers.draw(GL2.GL_TRIANGLES, shaderProgram2);
		else if (shaderSwitcher == 3) oglBuffers.draw(GL2.GL_TRIANGLES, shaderProgram3);
		
		//oglBuffers.draw(GL2.GL_TRIANGLE_FAN, shaderProgram);


	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		this.width = width;
		this.height = height;
		proj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 0.01, 1000.0);
	}

	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
			boolean deviceChanged) {
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
		ox = e.getX();
		oy = e.getY();
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseDragged(MouseEvent e) {
		cam = cam.addAzimuth((double) Math.PI * (ox - e.getX()) / width)
				.addZenith((double) Math.PI * (e.getY() - oy) / width);
		ox = e.getX();
		oy = e.getY();
	}

	public void mouseMoved(MouseEvent e) {
	}

	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_W:
			cam = cam.forward(1);
			break;
		case KeyEvent.VK_D:
			cam = cam.right(1);
			break;
		case KeyEvent.VK_S:
			cam = cam.backward(1);
			break;
		case KeyEvent.VK_A:
			cam = cam.left(1);
			break;
		case KeyEvent.VK_CONTROL:
			cam = cam.down(1);
			break;
		case KeyEvent.VK_SHIFT:
			cam = cam.up(1);
			break;
		case KeyEvent.VK_SPACE:
			cam = cam.withFirstPerson(!cam.getFirstPerson());
			break;
		case KeyEvent.VK_R:
			cam = cam.mulRadius(0.9f);
			break;
		case KeyEvent.VK_F:
			cam = cam.mulRadius(1.1f);
			break;
		}
	}

	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

	public void dispose(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();
		gl.glDeleteProgram(shaderProgram0);
		gl.glDeleteProgram(shaderProgram1);
		gl.glDeleteProgram(shaderProgram2);
		gl.glDeleteProgram(shaderProgram3);
	}
	
	public void addVariableToShaderPrograms(){
				// pro funkce
				locTime0 = gl.glGetUniformLocation(shaderProgram0, "time");
				locClosingIncrement0 = gl.glGetUniformLocation(shaderProgram0, "closing_increment");
				// prepinani funkci z gui
				locFunctionSwitcher0 = gl.glGetUniformLocation(shaderProgram0, "functionSwitcher");
				locMat0 = gl.glGetUniformLocation(shaderProgram0, "mat");
				locMatView0 = gl.glGetUniformLocation(shaderProgram0,"matView");
				locMatProjection0 = gl.glGetUniformLocation(shaderProgram0,"matProjection");
				// svetlo a barvy
				locLightPosition0 = gl.glGetUniformLocation(shaderProgram0, "lightPosition");
				locBaseColor0 = gl.glGetUniformLocation(shaderProgram0, "baseColor");
				locAmbientColor0 = gl.glGetUniformLocation(shaderProgram0, "Ambient");
				locDiffuseColor0 = gl.glGetUniformLocation(shaderProgram0, "Diffuse");
				locSpecularColor0 = gl.glGetUniformLocation(shaderProgram0, "Specular");
				// reflektor a utlum
				locSpotCutOff0 = gl.glGetUniformLocation(shaderProgram0, "spotCutOff");
				locConstantAtt0 = gl.glGetUniformLocation(shaderProgram0, "constantAttenuation");
				locLinearAtt0 = gl.glGetUniformLocation(shaderProgram0, "linearAttenuation");
				locQuadraticAtt0 = gl.glGetUniformLocation(shaderProgram0, "quadraticAttenuation");
				locCBumpSize0 = gl.glGetUniformLocation(shaderProgram0, "cBumpSize");
				
				// pro funkce
				locTime1 = gl.glGetUniformLocation(shaderProgram1, "time");
				locClosingIncrement1 = gl.glGetUniformLocation(shaderProgram1, "closing_increment");
				// prepinani funkci z gui
				locFunctionSwitcher1 = gl.glGetUniformLocation(shaderProgram1, "functionSwitcher");
				locMat1 = gl.glGetUniformLocation(shaderProgram1, "mat");
				locMatView1 = gl.glGetUniformLocation(shaderProgram1,"matView");
				locMatProjection1 = gl.glGetUniformLocation(shaderProgram1,"matProjection");
				// svetlo a barvy
				locLightPosition1 = gl.glGetUniformLocation(shaderProgram1, "lightPosition");
				locBaseColor1 = gl.glGetUniformLocation(shaderProgram1, "baseColor");
				locAmbientColor1 = gl.glGetUniformLocation(shaderProgram1, "Ambient");
				locDiffuseColor1 = gl.glGetUniformLocation(shaderProgram1, "Diffuse");
				locSpecularColor1 = gl.glGetUniformLocation(shaderProgram1, "Specular");
				// reflektor a utlum
				locSpotCutOff1 = gl.glGetUniformLocation(shaderProgram1, "spotCutOff");
				locConstantAtt1 = gl.glGetUniformLocation(shaderProgram1, "constantAttenuation");
				locLinearAtt1 = gl.glGetUniformLocation(shaderProgram1, "linearAttenuation");
				locQuadraticAtt1 = gl.glGetUniformLocation(shaderProgram1, "quadraticAttenuation");
				locCBumpSize1 = gl.glGetUniformLocation(shaderProgram1, "cBumpSize");
				
				// pro funkce
				locTime2 = gl.glGetUniformLocation(shaderProgram2, "time");
				locClosingIncrement2 = gl.glGetUniformLocation(shaderProgram2, "closing_increment");
				// prepinani funkci z gui
				locFunctionSwitcher2 = gl.glGetUniformLocation(shaderProgram2, "functionSwitcher");
				locMat2 = gl.glGetUniformLocation(shaderProgram2, "mat");
				locMatView2 = gl.glGetUniformLocation(shaderProgram2,"matView");
				locMatProjection2 = gl.glGetUniformLocation(shaderProgram2,"matProjection");
				// svetlo a barvy
				locLightPosition2 = gl.glGetUniformLocation(shaderProgram2, "lightPosition");
				locBaseColor2 = gl.glGetUniformLocation(shaderProgram2, "baseColor");
				locAmbientColor2 = gl.glGetUniformLocation(shaderProgram2, "Ambient");
				locDiffuseColor2= gl.glGetUniformLocation(shaderProgram2, "Diffuse");
				locSpecularColor2 = gl.glGetUniformLocation(shaderProgram2, "Specular");
				// reflektor a utlum
				locSpotCutOff2 = gl.glGetUniformLocation(shaderProgram2, "spotCutOff");
				locConstantAtt2 = gl.glGetUniformLocation(shaderProgram2, "constantAttenuation");
				locLinearAtt2 = gl.glGetUniformLocation(shaderProgram2, "linearAttenuation");
				locQuadraticAtt2 = gl.glGetUniformLocation(shaderProgram2, "quadraticAttenuation");
				locCBumpSize2 = gl.glGetUniformLocation(shaderProgram2, "cBumpSize");
				
				// pro funkce
				locTime3 = gl.glGetUniformLocation(shaderProgram3, "time");
				locClosingIncrement3 = gl.glGetUniformLocation(shaderProgram3, "closing_increment");
				// prepinani funkci z gui
				locFunctionSwitcher3 = gl.glGetUniformLocation(shaderProgram3, "functionSwitcher");
				locMat3 = gl.glGetUniformLocation(shaderProgram3, "mat");
				locMatView3 = gl.glGetUniformLocation(shaderProgram3,"matView");
				locMatProjection3 = gl.glGetUniformLocation(shaderProgram3,"matProjection");
				// svetlo a barvy
				locLightPosition3 = gl.glGetUniformLocation(shaderProgram3, "lightPosition");
				locBaseColor3 = gl.glGetUniformLocation(shaderProgram3, "baseColor");
				locAmbientColor3 = gl.glGetUniformLocation(shaderProgram3, "Ambient");
				locDiffuseColor3 = gl.glGetUniformLocation(shaderProgram3, "Diffuse");
				locSpecularColor3 = gl.glGetUniformLocation(shaderProgram3, "Specular");
				// reflektor a utlum
				locSpotCutOff3 = gl.glGetUniformLocation(shaderProgram3, "spotCutOff");
				locConstantAtt3 = gl.glGetUniformLocation(shaderProgram3, "constantAttenuation");
				locLinearAtt3 = gl.glGetUniformLocation(shaderProgram3, "linearAttenuation");
				locQuadraticAtt3 = gl.glGetUniformLocation(shaderProgram3, "quadraticAttenuation");
				locCBumpSize3 = gl.glGetUniformLocation(shaderProgram3, "cBumpSize");
		
		
	}
	
	private void addVariableToShaderDisplay(){
		gl.glUniform1f(locTime0, time);
		gl.glUniform1f(locClosingIncrement0, closing_increment);
		gl.glUniform1i(locFunctionSwitcher0, functionSwitcher); 
		// svetlo a barvy
		/*System.out.println("lightPositionX "+lightPositionX );
		System.out.println("lightPositionY "+lightPositionY );
		System.out.println("lightPositionZ "+lightPositionZ );*/
		gl.glUniform3f(locLightPosition0, this.lightPositionX, this.lightPositionY, this.lightPositionZ);
		gl.glUniform4f(locBaseColor0, this.baseColorR, this.baseColorG, this.baseColorB, this.baseColorA);
		gl.glUniform4f(locAmbientColor0, this.ambientR, this.ambientG, this.ambientB, this.ambientA);
		gl.glUniform4f(locDiffuseColor0, this.diffuseR, this.diffuseG, this.diffuseB, this.diffuseA);
		gl.glUniform4f(locSpecularColor0, this.specularR, this.specularG, this.specularB, this.specularA);
		// reflektor a utlum
		gl.glUniform1f(locSpotCutOff0, this.spotCutOff);
		gl.glUniform1f(locConstantAtt0, this.constantAtt);
		gl.glUniform1f(locLinearAtt0, this.linearAtt);
		gl.glUniform1f(locQuadraticAtt0, this.quadraticAtt);
		gl.glUniform2f(locCBumpSize0, this.cBumpSizeX, this.cBumpSizeY);
		// pÅ™idÃ¡nÃ­ view * projection matice pro vÃ½poÄ�?et pozice objektu
		gl.glUniformMatrix4fv(locMat0, 1, false,
				ToFloatArray.convert(cam.getViewMatrix().mul(proj)), 0);
		// přidání pohledové matice pro výpo�?et per vertex osvětlení
		gl.glUniformMatrix4fv(locMatView0, 1, false, ToFloatArray.convert(cam.getViewMatrix()),0);
		gl.glUniformMatrix4fv(locMatProjection0, 1, false, ToFloatArray.convert(proj),0);
		
		
		gl.glUniform1f(locTime1, time);
		gl.glUniform1f(locClosingIncrement1, closing_increment);
		gl.glUniform1i(locFunctionSwitcher1, functionSwitcher); 
		// svetlo a barvy
		gl.glUniform3f(locLightPosition1, this.lightPositionX, this.lightPositionY, this.lightPositionZ);
		gl.glUniform4f(locBaseColor1, this.baseColorR, this.baseColorG, this.baseColorB, this.baseColorA);
		gl.glUniform4f(locAmbientColor1, this.ambientR, this.ambientG, this.ambientB, this.ambientA);
		gl.glUniform4f(locDiffuseColor1, this.diffuseR, this.diffuseG, this.diffuseB, this.diffuseA);
		gl.glUniform4f(locSpecularColor1, this.specularR, this.specularG, this.specularB, this.specularA);
		// reflektor a utlum
		gl.glUniform1f(locSpotCutOff1, this.spotCutOff);
		gl.glUniform1f(locConstantAtt1, this.constantAtt);
		gl.glUniform1f(locLinearAtt1, this.linearAtt);
		gl.glUniform1f(locQuadraticAtt1, this.quadraticAtt);
		gl.glUniform2f(locCBumpSize1, this.cBumpSizeX, this.cBumpSizeY);
		// pÅ™idÃ¡nÃ­ view * projection matice pro vÃ½poÄ�?et pozice objektu
		gl.glUniformMatrix4fv(locMat1, 1, false,
				ToFloatArray.convert(cam.getViewMatrix().mul(proj)), 0);
		// přidání pohledové matice pro výpo�?et per vertex osvětlení
		gl.glUniformMatrix4fv(locMatView1, 1, false, ToFloatArray.convert(cam.getViewMatrix()),0);
		gl.glUniformMatrix4fv(locMatProjection1, 1, false, ToFloatArray.convert(proj),0);
		
		gl.glUniform1f(locTime2, time);
		gl.glUniform1f(locClosingIncrement2, closing_increment);
		gl.glUniform1i(locFunctionSwitcher2, functionSwitcher); 
		// svetlo a barvy
		gl.glUniform3f(locLightPosition2, this.lightPositionX, this.lightPositionY, this.lightPositionZ);
		gl.glUniform4f(locBaseColor2, this.baseColorR, this.baseColorG, this.baseColorB, this.baseColorA);
		gl.glUniform4f(locAmbientColor2, this.ambientR, this.ambientG, this.ambientB, this.ambientA);
		gl.glUniform4f(locDiffuseColor2, this.diffuseR, this.diffuseG, this.diffuseB, this.diffuseA);
		gl.glUniform4f(locSpecularColor2, this.specularR, this.specularG, this.specularB, this.specularA);
		// reflektor a utlum
		gl.glUniform1f(locSpotCutOff2, this.spotCutOff);
		gl.glUniform1f(locConstantAtt2, this.constantAtt);
		gl.glUniform1f(locLinearAtt2, this.linearAtt);
		gl.glUniform1f(locQuadraticAtt2, this.quadraticAtt);
		gl.glUniform2f(locCBumpSize2, this.cBumpSizeX, this.cBumpSizeY);
		// pÅ™idÃ¡nÃ­ view * projection matice pro vÃ½poÄ�?et pozice objektu
		gl.glUniformMatrix4fv(locMat2, 1, false,
				ToFloatArray.convert(cam.getViewMatrix().mul(proj)), 0);
		// přidání pohledové matice pro výpo�?et per vertex osvětlení
		gl.glUniformMatrix4fv(locMatView2, 1, false, ToFloatArray.convert(cam.getViewMatrix()),0);
		gl.glUniformMatrix4fv(locMatProjection2, 1, false, ToFloatArray.convert(proj),0);
		
		gl.glUniform1f(locTime3, time);
		gl.glUniform1f(locClosingIncrement3, closing_increment);
		gl.glUniform1i(locFunctionSwitcher3, functionSwitcher); 
		// svetlo a barvy
		gl.glUniform3f(locLightPosition3, this.lightPositionX, this.lightPositionY, this.lightPositionZ);
		gl.glUniform4f(locBaseColor3, this.baseColorR, this.baseColorG, this.baseColorB, this.baseColorA);
		gl.glUniform4f(locAmbientColor3, this.ambientR, this.ambientG, this.ambientB, this.ambientA);
		gl.glUniform4f(locDiffuseColor3, this.diffuseR, this.diffuseG, this.diffuseB, this.diffuseA);
		gl.glUniform4f(locSpecularColor3, this.specularR, this.specularG, this.specularB, this.specularA);
		// reflektor a utlum
		gl.glUniform1f(locSpotCutOff3, this.spotCutOff);
		gl.glUniform1f(locConstantAtt3, this.constantAtt);
		gl.glUniform1f(locLinearAtt3, this.linearAtt);
		gl.glUniform1f(locQuadraticAtt3, this.quadraticAtt);
		gl.glUniform2f(locCBumpSize3, this.cBumpSizeX, this.cBumpSizeY);
		// pÅ™idÃ¡nÃ­ view * projection matice pro vÃ½poÄ�?et pozice objektu
		gl.glUniformMatrix4fv(locMat3, 1, false,
				ToFloatArray.convert(cam.getViewMatrix().mul(proj)), 0);
		// přidání pohledové matice pro výpo�?et per vertex osvětlení
		gl.glUniformMatrix4fv(locMatView3, 1, false, ToFloatArray.convert(cam.getViewMatrix()),0);
		gl.glUniformMatrix4fv(locMatProjection3, 1, false, ToFloatArray.convert(proj),0);
		
		
	}

}