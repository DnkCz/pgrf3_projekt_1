package projekt;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;

public class MyFrame extends Frame {
	public MyFrame(String string) {
		super(string);
	}

	public Renderer renderer;

	TextField lightX, lightY, lightZ, baseR, baseG, baseB, baseA, ambientR, ambientG, ambientB, ambientA, diffuseR,
			diffuseG, diffuseB, diffuseA, specularR, specularG, specularB, specularA,

			spotCutOff, constantAtt, linearAtt, quadraticAtt,
			
			cBumpSizeX,cBumpSizeY
			;

	Button setDefaultOptions, setReflectorDefault, apply;

	Choice functionChooser,shaderSwitcher,glFront,glBack ;

	public void drawGui() {
		// kontejner pro funkce
		Container functionContainer = new Container();
		functionContainer.setLayout(new FlowLayout());
		functionChooser = new Choice();
		functionChooser.add("Roof [K]");
		functionChooser.add("Funnel [K]");
		functionChooser.add("Cylinder [C]");
		functionChooser.add("Lava [C]");
		functionChooser.add("Sphere [S]");
		functionChooser.add("ClosingSphere [S]");
		functionContainer.add(functionChooser);
		
		shaderSwitcher = new Choice();
		shaderSwitcher.add("PerVertex");
		shaderSwitcher.add("PerPixel");
		shaderSwitcher.add("NormalMapping");
		shaderSwitcher.add("ParallaxMapping");
		functionContainer.add(shaderSwitcher);
		
		
		functionContainer.add(new Label("cBumpSize"));
		cBumpSizeX = new TextField();
		cBumpSizeY = new TextField();
		functionContainer.add(cBumpSizeX);
		functionContainer.add(cBumpSizeY);
		
		functionContainer.add(new Label("GL_FRONT"));
		glFront = new Choice();
		glFront.add("FILL");
		glFront.add("LINE");;
		functionContainer.add(glFront);

		functionContainer.add(new Label("GL_BACK"));
		glBack = new Choice();
		glBack.add("FILL");
		glBack.add("LINE");;
		functionContainer.add(glBack);
		

		// kontejner pro nastaveni
		Container optionsContainer = new Container();
		optionsContainer.setLayout(new FlowLayout());

		// lightposition //lightPosition = vec3(1.0,1.0,1.0);
		optionsContainer.add(new Label("lightPosition [1.0,1.0,1.0]"));
		lightX = new TextField();
		lightY = new TextField();
		lightZ = new TextField();
		optionsContainer.add(lightX);
		optionsContainer.add(lightY);
		optionsContainer.add(lightZ);

		// Base color //vec4 Ambient = vec4(0.13,0.2,0.49,1.0);
		optionsContainer.add(new Label("BaseColor [0.13,0.2,0.49.1.0]"));
		baseR = new TextField();
		baseG = new TextField();
		baseB = new TextField();
		baseA = new TextField();
		optionsContainer.add(baseR);
		optionsContainer.add(baseG);
		optionsContainer.add(baseB);
		optionsContainer.add(baseA);

		// Ambient // vec4 Ambient = vec4(0.13,0.2,0.49,1.0);
		optionsContainer.add(new Label("Ambient [0.13,0.2,0.49,1.0]"));
		ambientR = new TextField();
		ambientG = new TextField();
		ambientB = new TextField();
		ambientA = new TextField();
		optionsContainer.add(ambientR);
		optionsContainer.add(ambientG);
		optionsContainer.add(ambientB);
		optionsContainer.add(ambientA);

		// Diffuse //vec4 Diffuse = vec4(0.7,0.16,0.62,1.0);
		optionsContainer.add(new Label("Diffuse [0.7,0.16,0.62,1.0]"));
		diffuseR = new TextField();
		diffuseG = new TextField();
		diffuseB = new TextField();
		diffuseA = new TextField();
		optionsContainer.add(diffuseR);
		optionsContainer.add(diffuseG);
		optionsContainer.add(diffuseB);
		optionsContainer.add(diffuseA);

		// Specular //vec4 Specular = vec4(0.1,0.9,0.6,1.0);
		optionsContainer.add(new Label("Specular [0.1,0.9,0.6,1.0]"));
		specularR = new TextField();
		specularG = new TextField();
		specularB = new TextField();
		specularA = new TextField();
		optionsContainer.add(specularR);
		optionsContainer.add(specularG);
		optionsContainer.add(specularB);
		optionsContainer.add(specularA);

		setDefaultOptions = new Button("Set default");
		optionsContainer.add(setDefaultOptions);

		// reflector kontejner //float spotCutOff = 0.5;
		Container reflectorContainer = new Container();
		reflectorContainer.setLayout(new FlowLayout());
		reflectorContainer.add(new Label("SpotCutOff [0.5]"));
		spotCutOff = new TextField();
		reflectorContainer.add(spotCutOff);

		// konstantni utlum //float constantAttenuation = 0.90;
		reflectorContainer.add(new Label("Constant attenuation [0.98]"));
		constantAtt = new TextField();
		reflectorContainer.add(constantAtt);

		// linearni utlum //float linearAttenuation = 0.01;
		reflectorContainer.add(new Label("Linear attenuation [0.01]"));
		linearAtt = new TextField();
		reflectorContainer.add(linearAtt);

		// kvadraticky utlum float quadraticAttenuation =0.001;
		reflectorContainer.add(new Label("Quadratic attenuation [0.001]"));
		quadraticAtt = new TextField();
		reflectorContainer.add(quadraticAtt);

		// default reflektor hodnoty
		setReflectorDefault = new Button("Set default");
		reflectorContainer.add(setReflectorDefault);

		Container bottomContainer = new Container();

		bottomContainer.setLayout(new BorderLayout());
		bottomContainer.add("North", functionContainer);
		bottomContainer.add("Center", optionsContainer);
		bottomContainer.add("South", reflectorContainer);
		this.add("Center", bottomContainer);

		// applyButton
		/*Container applyCont = new Container();
		applyCont.setLayout(new FlowLayout());
		apply = new Button("Apply changes");
		applyCont.add(apply);
		this.add("South", applyCont);*/

		// listenery
		this.initListeners();
		
		
		// def hodnoty
		//this.setDefaultAtt();
		//this.setDefaultLight();

	}

	private void initListeners() {

		functionChooser.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				int index = functionChooser.getSelectedIndex();
				System.out.println("index je " + index);
				renderer.functionSwitcher = index;

			}
		});
		
		shaderSwitcher.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				int index = shaderSwitcher.getSelectedIndex();
				System.out.println("B ren.shsw "+renderer.shaderSwitcher);
				renderer.shaderSwitcher = index;
				System.out.println("A ren.shsw "+renderer.shaderSwitcher);
				System.out.println("index "+index);
				
			}
		});
		
		cBumpSizeX.addTextListener(new TextListener() {
			
			@Override
			public void textValueChanged(TextEvent e) {
				String str = cBumpSizeX.getText().toString();
				if (!str.isEmpty()) {
					try {
						renderer.cBumpSizeX = Float.parseFloat(str);
					}catch(NumberFormatException ex){
						
					}
					
					// System.out.println("string " + checkFloat(str)); 
				}

				
			}
		});
		
		cBumpSizeY.addTextListener(new TextListener() {
			
			@Override
			public void textValueChanged(TextEvent e) {
				String str = cBumpSizeY.getText().toString();
				if (!str.isEmpty()) {
					try {
						renderer.cBumpSizeY = Float.parseFloat(str);
					}catch(NumberFormatException ex){
						
					}
					
					// System.out.println("string " + checkFloat(str)); 
				}

				
			}
		});
		
		
		glFront.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				int index = glFront.getSelectedIndex();
				renderer.glFront = index;
				
			}
		});
		
		glBack.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				int index = glBack.getSelectedIndex();
				renderer.glBack = index;
				
			}
		});

	lightX.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = lightX.getText().toString();
				if (!str.isEmpty()) {
					//renderer.lightPositionX = checkFloat(str);
					try {
					renderer.lightPositionX = Float.parseFloat(str);
					} catch (NumberFormatException ex){
					
					}
					// System.out.println("string " + checkFloat(str)); 
				}

			}
		});

		lightY.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = lightY.getText().toString();
				if (!str.isEmpty()) {
					//renderer.lightPositionY = checkFloat(str);
					
					try{
						renderer.lightPositionY = Float.parseFloat(str);
						
					}catch(NumberFormatException ex) {
						
					}

				}

			}
		});

		lightZ.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = lightZ.getText().toString();
				if (!str.isEmpty()) {
					try{
						renderer.lightPositionZ = Float.parseFloat(str);
						
					}catch(NumberFormatException ex) {
						
					}
				}

			}
		});
		
		baseR.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = baseR.getText().toString();
				if (!str.isEmpty()) {
					renderer.baseColorR = checkFloat(str);
				}

			}
		});
		baseG.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = baseG.getText().toString();
				if (!str.isEmpty()) {
					renderer.baseColorG = checkFloat(str);
				}

			}
		});
		
		baseB.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = baseB.getText().toString();
				if (!str.isEmpty()) {
					renderer.baseColorB = checkFloat(str);
				}

			}
		});
		
		baseA.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = baseA.getText().toString();
				if (!str.isEmpty()) {
					renderer.baseColorA = checkFloat(str);
				}

			}
		});
		
		ambientR.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = ambientR.getText().toString();
				if (!str.isEmpty()) {
					renderer.ambientR = checkFloat(str);
				}

			}
		});
		
		ambientG.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = ambientG.getText().toString();
				if (!str.isEmpty()) {
					renderer.ambientG = checkFloat(str);
				}

			}
		});
		
		ambientB.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = ambientB.getText().toString();
				if (!str.isEmpty()) {
					renderer.ambientB = checkFloat(str);
				}

			}
		});
		
		ambientA.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = ambientA.getText().toString();
				if (!str.isEmpty()) {
					renderer.ambientA = checkFloat(str);
				}

			}
		});
		
		diffuseR.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = diffuseR.getText().toString();
				if (!str.isEmpty()) {
					renderer.diffuseR = checkFloat(str);
				}

			}
		});
		
		diffuseG.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = diffuseG.getText().toString();
				if (!str.isEmpty()) {
					renderer.diffuseG = checkFloat(str);
				}

			}
		});
		
		diffuseB.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = diffuseB.getText().toString();
				if (!str.isEmpty()) {
					renderer.diffuseB = checkFloat(str);
				}

			}
		});
		
		diffuseA.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = diffuseA.getText().toString();
				if (!str.isEmpty()) {
					renderer.diffuseA = checkFloat(str);
				}

			}
		});
		
		specularR.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = specularR.getText().toString();
				if (!str.isEmpty()) {
					renderer.specularR = checkFloat(str);
				}

			}
		});
		
		specularG.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = specularG.getText().toString();
				if (!str.isEmpty()) {
					renderer.specularG = checkFloat(str);
				}

			}
		});

		
		specularB.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = specularB.getText().toString();
				if (!str.isEmpty()) {
					renderer.specularB = checkFloat(str);
				}

			}
		});

		
		specularA.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = specularA.getText().toString();
				if (!str.isEmpty()) {
					renderer.specularA = checkFloat(str);
				}

			}
		});
		
		spotCutOff.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = spotCutOff.getText().toString();
				if (!str.isEmpty()) {
					renderer.spotCutOff = checkFloat(str);
				}

			}
		});

		constantAtt.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = constantAtt.getText().toString();
				if (!str.isEmpty()) {
					renderer.constantAtt = checkFloat(str);
				}

			}
		});

		linearAtt.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = linearAtt.getText().toString();
				if (!str.isEmpty()) {
					renderer.linearAtt = checkFloat(str);
				}

			}
		});

		quadraticAtt.addTextListener(new TextListener() {

			@Override
			public void textValueChanged(TextEvent e) {
				String str = quadraticAtt.getText().toString();
				if (!str.isEmpty()) {
					renderer.quadraticAtt = checkFloat(str);
				}

			}
		});
		
		setDefaultOptions.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setDefaultLight();
				
			}
		});
		
		setReflectorDefault.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setDefaultAtt();
				
			}
		});


	}
	

	private float checkFloat(String number) {
		try {
			float f = Float.parseFloat(number);

			if (f < 0.0f) {
				System.out.println("number lower than 0.0f");
				f= 0.45f;
			}

			if (f > 1.0f) {
				System.out.println("number higher than 1.0f");
				f = 0.65f;
			}

			return f;
		} catch (NumberFormatException e) {
			System.out.println("number " + number + " is not float, setting to 0.75f");
			return 0.75f;
		}

	}

	private void setDefaultLight() {
		
		//renderer.lightPositionX = 1.0f;
		lightX.setText("1.0");
		//renderer.lightPositionY = 1.0f;
		lightY.setText("1.0");
		//renderer.lightPositionZ = 1.0f;
		lightZ.setText("1.0");

		//renderer.baseColorR = 0.13f;
		baseR.setText("0.13");
		//renderer.baseColorG = 0.2f;
		baseG.setText("0.2");
		//renderer.baseColorB = 0.49f;
		baseB.setText("0.13");
		//renderer.baseColorA = 1.0f;
		baseA.setText("0.13");

		//renderer.ambientR = 0.13f;
		ambientR.setText("0.13");
		//renderer.ambientG = 0.2f;
		ambientG.setText("0.2");
		//renderer.ambientB = 0.49f;
		ambientB.setText("0.49");
		//renderer.ambientA = 1.0f;
		ambientA.setText("1.0");

		//renderer.diffuseR = 0.7f;
		diffuseR.setText("0.7");
		//renderer.diffuseG = 0.16f;
		diffuseG.setText("0.16");
		//renderer.diffuseB = 0.62f;
		diffuseB.setText("0.62");
		//renderer.diffuseA = 1.0f;
		diffuseA.setText("1.0");

		//renderer.specularR = 0.1f;
		specularR.setText("0.1");
		//renderer.specularG = 0.9f;
		specularG.setText("0.9");
		//renderer.specularB = 0.6f;
		specularB.setText("0.6");
		//renderer.specularA = 1.0f;
		specularA.setText("1.0");
		
		/*functionChooser.select(1);
		shaderSwitcher.select(3);*/
		
	}

	private void setDefaultAtt() {
		spotCutOff.setText("0.5");
		//renderer.spotCutOff = 0.5f;
		constantAtt.setText("0.98");
		//renderer.constantAtt = 0.98f;
		linearAtt.setText("0.1");
		//renderer.linearAtt = 0.01f;
		quadraticAtt.setText("0.001");
		//renderer.quadraticAtt = 0.001f;
		
		cBumpSizeX.setText("0.04");
		cBumpSizeY.setText("-0.02");
	
	}
	
	public void setDefaultValues(){
		this.setDefaultAtt();
		this.setDefaultLight();
	}

	public Renderer getRenderer() {
		return this.renderer;
	}

	public void setRenderer(Renderer renderer) {
		this.renderer = renderer;
	}

}
