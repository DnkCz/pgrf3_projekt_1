package projekt;



import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;



public class JOGLApp {
	static final int FPS = 60; // animator's target frames per second
	
	public static void main(String[] args) {
		try {
			MyFrame testFrame = new MyFrame("TestFrame");
			testFrame.setSize(512, 384);
			testFrame.setLayout(new BorderLayout());
			// setup OpenGL Version 2
	    	GLProfile profile = GLProfile.get(GLProfile.GL2);
	    	GLCapabilities capabilities = new GLCapabilities(profile);
	    	
	    	// The canvas is the widget that's drawn in the JFrame
	    	GLCanvas canvas = new GLCanvas(capabilities);
	    	Renderer ren = new Renderer();
			canvas.addGLEventListener(ren);
			canvas.addMouseListener(ren);
			canvas.addMouseMotionListener(ren);
			canvas.addKeyListener(ren);
	    	canvas.setSize( 1024, 500);
	    	
	    	// predani reference na renderer
	    	testFrame.setRenderer(ren);
	    	
	    	// vykreslení vlastního gui
	    	testFrame.drawGui();
	    	
	    	
	    	
	    	// platno
	    	testFrame.add("North",canvas);
	    	
	    	// shutdown the program on windows close event
	        			
	    	//final Animator animator = new Animator(canvas);
	    	final FPSAnimator animator = new FPSAnimator(canvas, FPS, true);
	    	 
	    	testFrame.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					new Thread() {
	                     @Override
	                     public void run() {
	                        if (animator.isStarted()) animator.stop();
	                        System.exit(0);
	                     }
	                  }.start();
				}
			});
	    	//testFrame.setTitle("");
	    	testFrame.pack();
	    	testFrame.setVisible(true);
	    	
	    	animator.start(); // start the animation loop
	    	testFrame.setDefaultValues();
            
	    	
	    	
            
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}