package glsl01_start_0;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

/**
* Ukazka pro praci s shadery v GLSL:
* nacteni shaderu ze Stringu,
* upraveno pro JOGL 2.3.0 a vyssi
* 
* @author PGRF FIM UHK
* @version 2.0
* @since   2015-09-05 
*/
public class Renderer implements GLEventListener, MouseListener,
		MouseMotionListener, KeyListener {

	int width, height;

	int shaderProgram;

	public void init(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();

		System.out.println("Init GL is " + gl.getClass().getName());
		System.out.println("OpenGL version " + gl.glGetString(GL2.GL_VERSION));
		System.out.println("OpenGL vendor " + gl.glGetString(GL2.GL_VENDOR));
		System.out
				.println("OpenGL renderer " + gl.glGetString(GL2.GL_RENDERER));
		System.out.println("OpenGL extension "
				+ gl.glGetString(GL2.GL_EXTENSIONS));

		// overeni podpory shaderu
		String extensions = gl.glGetString(GL2.GL_EXTENSIONS);
		if (extensions.indexOf("GL_ARB_vertex_shader") == -1
				|| extensions.indexOf("GL_ARB_fragment_shader") == -1) {
			throw new RuntimeException("Shaders not available.");
		}

		createShaders(gl);
	}
	
	void createShaders(GL2 gl) {
		String shaderVertSrc[] = { 
			"#version 330\n",
			"in vec2 inPosition;", // vstup z vertex bufferu
			"void main() {", 
			"	vec2 position = inPosition;",
			"   position.x += 0.1;",
			" 	gl_Position = vec4(position, 0.0, 1.0);", 
			"}" 
		};
		// gl_Position - vestavena vystupni promenna pro pozici vrcholu
		// pred orezanim w a dehomogenizaci, musi byt naplnena

		// fragCoord je skute�n� pozice ve screenu (���e je 512)
		String shaderFragSrc[] = { 
			"#version 330\n",
			"out vec4 outColor;", // vystup z fragment shaderu
			"void main() {",
			" 	outColor = vec4(gl_FragCoord.x/512,0.1,0.8, 1.0);", 
			"}" 
		};

		// vertex shader
		int vs = gl.glCreateShader(GL2.GL_VERTEX_SHADER);
		gl.glShaderSource(vs, shaderVertSrc.length, shaderVertSrc,
				(int[]) null, 0);
		gl.glCompileShader(vs);
		System.out.println("Compile VS error: " + checkLogInfo(gl, vs, GL2.GL_COMPILE_STATUS));

		// fragment shader
		int fs = gl.glCreateShader(GL2.GL_FRAGMENT_SHADER);
		gl.glShaderSource(fs, shaderFragSrc.length, shaderFragSrc,
				(int[]) null, 0);
		gl.glCompileShader(fs);
		System.out.println("Compile FS error: " + checkLogInfo(gl, fs, GL2.GL_COMPILE_STATUS));

		// sestaveni programu
		shaderProgram = gl.glCreateProgram(); // obecn� vytvo�en� programu
		gl.glAttachShader(shaderProgram, vs); // p�i�azen� zkompulovan�ho shaderu
		gl.glAttachShader(shaderProgram, fs);
		gl.glLinkProgram(shaderProgram); // prolinkov�n� shader�
		System.out.println("Link error: " + checkLogInfo(gl, shaderProgram, GL2.GL_LINK_STATUS));
		
		if (vs >0) gl.glDetachShader(shaderProgram, vs);
		if (fs >0) gl.glDetachShader(shaderProgram, fs);
		if (vs >0) gl.glDeleteShader(vs);
		if (fs >0) gl.glDeleteShader(fs);
	
	}

	public void display(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();
		gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

		// nastaveni aktualniho shaderu, v teto ukazce nadbytecne
		gl.glUseProgram(shaderProgram); // pro pouziti vychoziho shaderu
										// "fixni pajplajny" slouzi
										// gl.glUseProgram(0);

		// vykresleni
		gl.glBegin(GL2.GL_TRIANGLES);
		gl.glVertex2f(-1f, -1);
		gl.glVertex2f(1, 0);
		gl.glVertex2f(0, 1);
		gl.glEnd();

	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		this.width = width;
		this.height = height;
	}

	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
			boolean deviceChanged) {
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseDragged(MouseEvent e) {
	}

	public void mouseMoved(MouseEvent e) {
	}

	public void keyPressed(KeyEvent e) {
	}

	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

	public void dispose(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();
		gl.glDeleteProgram(shaderProgram);
	}

	static private String checkLogInfo(GL2 gl, int programObject, int mode) {
		switch (mode) {
		case GL2.GL_COMPILE_STATUS:
			return checkLogInfoShader(gl, programObject, mode);
		case GL2.GL_LINK_STATUS:
		case GL2.GL_VALIDATE_STATUS:
			return checkLogInfoProgram(gl, programObject, mode);
		default:
			return "Unsupported mode.";
		}
	}

	static private String checkLogInfoShader(GL2 gl, int programObject, int mode) {
		int[] error = new int[] { -1 };
		gl.glGetShaderiv(programObject, mode, error, 0);
		if (error[0] != GL2.GL_TRUE) {
			int[] len = new int[1];
			gl.glGetShaderiv(programObject, GL2.GL_INFO_LOG_LENGTH, len, 0);
			if (len[0] == 0) {
				return null;
			}
			byte[] errorMessage = new byte[len[0]];
			gl.glGetShaderInfoLog(programObject, len[0], len, 0, errorMessage,
					0);
			return new String(errorMessage, 0, len[0]);
		}
		return null;
	}

	static private String checkLogInfoProgram(GL2 gl, int programObject, int mode) {
		int[] error = new int[] { -1 };
		gl.glGetProgramiv(programObject, mode, error, 0);
		if (error[0] != GL2.GL_TRUE) {
			int[] len = new int[1];
			gl.glGetProgramiv(programObject, GL2.GL_INFO_LOG_LENGTH, len, 0);
			if (len[0] == 0) {
				return null;
			}
			byte[] errorMessage = new byte[len[0]];
			gl.glGetProgramInfoLog(programObject, len[0], len, 0, errorMessage,
					0);
			return new String(errorMessage, 0, len[0]);
		}
		return null;
	}
}