package glsl01_start_5;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

import oglutils.OGLBuffers;
import oglutils.OGLTextRenderer;
import oglutils.OGLUtils;
import oglutils.ShaderUtils;

/**
 * GLSL sample:<br/>
 * Draw two different geometries with two different shader programs<br/>
 * Requires JOGL 2.3.0 or newer
 * 
 * @author PGRF FIM UHK
 * @version 2.0
 * @since 2015-09-05
 */
public class Renderer implements GLEventListener, MouseListener,
		MouseMotionListener, KeyListener {

	int width, height;

	OGLBuffers buffers, buffers2;
	OGLTextRenderer textRenderer = new OGLTextRenderer();
	
	int shaderProgram, shaderProgram2, locTime, locTime2;

	float time = 0;

	
	public void init(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();
		
		OGLUtils.printOGLparameters(gl);
		OGLUtils.shaderCheck(gl);
		
		// shader files are in /res/shader/ directory
		// res directory must be set as a source directory of the project
		// e.g. in Eclipse via main menu Project/Properties/Java Build Path/Source
		shaderProgram = ShaderUtils.loadProgram(gl, "/shader/glsl01/start");
		shaderProgram2 = ShaderUtils.loadProgram(gl, "/shader/glsl01/start2");
		createBuffers(gl);
		
		locTime = gl.glGetUniformLocation(shaderProgram, "time");
		locTime2 = gl.glGetUniformLocation(shaderProgram2, "time"); 
		
	}
	
	void createBuffers(GL2 gl) {
		float[] vertexBufferData = {
			-1, -1, 	0.7f, 0, 0, 
			 1,  0,		0, 0.7f, 0,
			 0,  1,		0, 0, 0.7f 
		};
		int[] indexBufferData = { 0, 1, 2 };
		OGLBuffers.Attrib[] attributes = {
				new OGLBuffers.Attrib("inPosition", 2),
				new OGLBuffers.Attrib("inColor", 3)
		};
		buffers = new OGLBuffers(gl, vertexBufferData, attributes,
				indexBufferData);
		
		float[] vertexBufferDataPos = {
			-1, 1, 
			0.5f, 0,
			-0.5f, -1 
		};
		float[] vertexBufferDataCol = {
			0, 1, 1, 
			1, 0, 1,
			1, 1, 1 
		};
		OGLBuffers.Attrib[] attributesPos = {
				new OGLBuffers.Attrib("inPosition", 2),
		};
		OGLBuffers.Attrib[] attributesCol = {
				new OGLBuffers.Attrib("inColor", 3)
		};
		buffers2 = new OGLBuffers(gl, vertexBufferDataPos, attributesPos,
				indexBufferData);
		buffers2.addVertexBuffer(vertexBufferDataCol, attributesCol);

	}

	public void display(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();
		
		gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

		time += 0.1;
		// set the current shader to be used
		gl.glUseProgram(shaderProgram); 
		gl.glUniform1f(locTime, time); // correct shader must be set before this

		// set the current shader to be used
		gl.glUseProgram(shaderProgram2); 
		gl.glUniform1f(locTime2, time); // correct shader must be set before this
		
		// bind and draw
		buffers.draw(GL2.GL_TRIANGLES, shaderProgram);
		buffers2.draw(GL2.GL_TRIANGLES, shaderProgram2);
		
		String text = new String(this.getClass().getName());
		textRenderer.drawStr2D(glDrawable, 3, height - 20, text);
		textRenderer.drawStr2D(glDrawable, width - 90, 3, " (c) PGRF UHK");

	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		this.width = width;
		this.height = height;
	}

	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
			boolean deviceChanged) {
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseDragged(MouseEvent e) {
	}

	public void mouseMoved(MouseEvent e) {
	}

	public void keyPressed(KeyEvent e) {
	}

	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

	public void dispose(GLAutoDrawable glDrawable) {
		GL2 gl = glDrawable.getGL().getGL2();
		gl.glDeleteProgram(shaderProgram);
		gl.glDeleteProgram(shaderProgram2);
	}

}