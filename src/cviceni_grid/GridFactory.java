/*
 * zkusit upravit na triangleStrip
 * 
 * 
 * */
package cviceni_grid;

import com.jogamp.opengl.GL2;

import oglutils.OGLBuffers;
import oglutils.ToFloatArray;

public class GridFactory {
	
	public static OGLBuffers createBuffer(GL2 gl,int m,int n){

		int vertices = m*n;
		int triangles = (m-1) * (n-1) * 2;
		int pocitadlo = 0;
		
		
		
		float [] vb = new float[2*vertices];
		int[] ib = new int [3*triangles];
		
		for (int i = 0; i< m; i++){
			for (int j = 0; j < n; j++){
				vb[(i*m+j)*2] =  (float) i/m;
				vb[(i*m+j)*2+1] =  (float) j/n;
			}
		}
		pocitadlo = 0;
		for (int k = 0; k < m-1; k++){
			for (int j = 0; j < n-1; j++){
				int i = k*m + j;
				
				ib[pocitadlo] = i;
				ib[pocitadlo+1] = i + n ;
				ib[pocitadlo+2] = i + n + 1;
				ib[pocitadlo+3] = i;
				ib[pocitadlo+4] = i + n + 1;
				ib[pocitadlo+5] = i + 1;
				
				pocitadlo = pocitadlo + 6; 		 
		}
		}
		
		OGLBuffers.Attrib[] attributes = {
				new OGLBuffers.Attrib("inPosition", 2)};
				
		return new OGLBuffers(gl, ToFloatArray.convert(vb), attributes, ib);
	}
	
	// vytiskne vertex a index buffer
	public static void printBuffers(float [] vertBuffer,int[] indexBuffer){
		System.out.println("VERTEX BUFFER");
		for (int i = 0; i < vertBuffer.length; i++) {
			System.out.println(vertBuffer[i]+" ");
		}
		
		System.out.println("INDEX BUFFER");
		for (int i = 0; i < indexBuffer.length; i++) {
			System.out.println(indexBuffer[i]+ " ");
			
		}
		
	}

}
