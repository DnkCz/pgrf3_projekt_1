#version 150
in vec2 inPosition; // input from the vertex buffer
in vec3 inColor; // input from the vertex buffer
out vec3 vertColor; // output from this shader to the next pipleline stage
uniform float time; // variable constant for all vertices in a single draw
uniform mat4 mat;


out vec3 lightVector;
out vec3 viewVector;
out vec3 normal;
out vec2 texCoord;

float funkce(vec2 position) {
return cos(sqrt(position.x * position.x + position.y*position.y));
}

// difference 
vec3 normalDiff (vec2 uv){
float delta = 0.01;
vec3 n;
n.z = 1;
n.x = (funkce(uv-vec2(delta,0.0)) - (funkce(uv+vec2(delta,0.0)) )) /(2.0*delta);
n.y = (funkce(uv-vec2(0.0,delta)) - (funkce(uv+vec2(0.0,delta)) )) / (2.0*delta);

return n;
}


void main() {
	vec2 position = inPosition;
	texCoord = inPosition;
	//position.x += 0.1;
	//position.y += cos(position.x + time);
	
	position -=0.5;
	position *=2;
	position *=10;

	float z = funkce(position.xy);
	
	normal = normalDiff(position);
	lightVector = vec3(1.0,1.0,10.0);
	
	 
	
	gl_Position = mat * vec4(position,z,1.0);
	vertColor = vec3(position,1.0);
} 