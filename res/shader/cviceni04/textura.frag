#version 150
in vec3 vertColor; // input from the previous pipeline stage
out vec4 outColor; // output from the fragment shader

in vec3 lightVector;
in vec3 viewVector;
in vec3 normal;
in vec2 texCoord;
uniform sampler2D texture;
 
void main() {

outColor = vec4(texCoord,0.0,1.0);
outColor = texture2D(texture, texCoord);

} 