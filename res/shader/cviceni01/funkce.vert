#version 150
in vec2 inPosition; // input from the vertex buffer
in vec3 inColor; // input from the vertex buffer
out vec3 vertColor; // output from this shader to the next pipleline stage
uniform float time; // variable constant for all vertices in a single draw
uniform mat4 mat;
void main() {
	vec2 position = inPosition;
	//position.x += 0.1;
	//position.y += cos(position.x + time);
	
	position -=0.5;
	position *=2;
	position *=10;
	float z = cos(sqrt(position.x * position.x + position.y*position.y));
	
	gl_Position = mat * vec4(position,z,1.0);
	vertColor = vec3(position,1.0);
} 