#version 150
in vec3 vertColor; // input from the previous pipeline stage
out vec4 outColor; // output from the fragment shader

in vec3 lightVector;
in vec3 viewVector;
in vec3 normal;
 
void main() {

float diffuse = max(dot( normalize(lightVector),normalize(normal)),0.0 );
outColor = vec4(normalize(normal),1.0);

outColor = vec4(vec3(diffuse),1.0); 
} 