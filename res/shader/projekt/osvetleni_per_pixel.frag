#version 150
out vec4 outColor; // output from the fragment shader

in vec3 lightVector;
in vec3 viewVector;
in vec3 normal;

uniform vec4 Diffuse;
 
void main() {

float NdotL = max(dot( normalize(lightVector),normalize(normal)),0.0 );
 gl_FragColor = NdotL * Diffuse;
} 