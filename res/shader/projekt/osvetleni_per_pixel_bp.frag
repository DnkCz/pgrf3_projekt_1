#version 150
in vec3 vertColor; // input from the previous pipeline stage
out vec4 outColor; // output from the fragment shader

in vec3 lightVector;
in vec3 viewVector;
in vec3 normal;

in float distanceFromLight;
 
// todo poslat z appky
uniform vec4 Ambient;
uniform vec4 BaseColor;
uniform vec4 Diffuse;
uniform vec4 Specular;
// asi chyba v prednaskach power bude spis float
const float SpecularPower = 0.3;

// todo zase predat z appky
// koeficienty utlumu svetla
uniform float constantAttenuation;
uniform float linearAttenuation;
uniform float quadraticAttenuation;

// todo z appky
const float spotCutOff = 0.5;
 
// smer svetla 
vec3 spotDirection =vec3(1.0,1.0,-1.0);
 

void main() {

vec3 ld = normalize(lightVector);
vec3 nd = normalize(normal);
vec3 vd = normalize(viewVector);

float NDotL = max(dot( normalize(lightVector),normalize(normal)),0.0 );

// odraz spocitan dle prednasky reflection = 2*(L . N) * N  - L
vec3 reflection = normalize(2*NDotL * (nd) -ld);
float RDotV = max(dot( normalize(reflection),normalize(vd)),0.0 );

// dle prednasky h = ( L+E )/ | L+E | .. ale chceme normalizovaný
vec3 halfVector = normalize((ld+vd)/length(ld+vd)); 


float NDotH = max(dot( normalize(normal),normalize(halfVector)),0.0 );


vec4 totalAmbient = Ambient + BaseColor;
vec4 totalDiffuse = Diffuse * NDotL *BaseColor;
vec4 totalSpecular = Specular *(pow (NDotH,SpecularPower * 4.0));

// vypocet utlumu
float attenuation = 1.0/(
			constantAttenuation 
			+ linearAttenuation * distanceFromLight 
			+ quadraticAttenuation*distanceFromLight*distanceFromLight
			);
			

float spotEffect = dot(normalize(spotDirection),normalize(-ld));
if (spotEffect > spotCutOff){
	gl_FragColor = totalAmbient + attenuation*(totalDiffuse + totalSpecular);
} else {
	gl_FragColor = totalAmbient;  
}


} 