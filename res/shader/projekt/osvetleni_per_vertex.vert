#version 150

in vec2 inPosition; // input from the vertex buffer

uniform mat4 mat;
uniform mat4 matProjection;
uniform mat4 matView;

uniform float time; 
uniform float closing_increment;

uniform int functionSwitcher; 


out vec3 lightVector;
out vec3 viewVector;
out vec3 normal;

out vec4 perVertexColor; 

// todo poslat z appky 
const vec4 lightPosition = vec4(-10.0,5.0,2.0,1.0);
uniform vec4 Ambient; //vec4 (0.4,0.0,0.0,0.0);
uniform vec4 Diffuse; //vec4(1.0,1.0,1.0,1.0);

const float delta = 0.01;
const float PI = 3.14159265359;

/*
*FUNKCE 
*[K] - kartezske souradnice
*[S] - sfericeke souradnice
*[C] - cylindricke souradnice
*/

// [K] strecha
vec3 funkce_k1_strecha(vec2 position) {
vec3 pos;
pos.x = position.x;
pos.y = position.y;
pos.z =  cos(sqrt(position.x * position.x + position.y*position.y));
return pos;
}

// [K] trychtyr
vec3 funkce_k2_trychtyr(vec2 position){ 
	float s = position.x * 2 * PI;
	float t = position.y * 2 - 1;
	vec3 pos;
	pos.x = t*cos(s);
	pos.y = t*sin(s);
	pos.z = t;
	return pos;
}


// [C] valec
vec3 funkce_c1_valec(vec2 position){
    float t  = (position.x)*2*PI;
    float theta = (position.y)*2*PI;
    float r = t;
    vec3 pos;
    pos.x = r*cos(theta);
    pos.y = r*sin(theta);
    pos.z = sin(t);
    return pos; 	
}


// [C] vlastni "lava function"
vec3 funkce_c2_lava(vec2 position){
    float t  = (position.x)*time;
    float theta = (position.y)*PI/4 + PI;
    float r = t;
    vec3 pos;
    pos.x = r*cos(theta);
    pos.y = r*sin(theta);
    pos.z = sin(t);
    return pos; 	
}


// [S] sfera 
vec3 funkce_s1_sfera(vec2 position){
    float rho = 1;
    float phi = position.y * PI;
    float theta = position.x * 2 * PI;
    
    vec3 pos;
    pos.x = rho * sin(phi) * cos(theta);
    pos.y = rho * sin(phi) * sin(theta);
    pos.z = rho * cos(phi);
    
    return pos;
}

// [S] closing sfere 
vec3 funkce_s2_closing_sfere(vec2 position){
    float rho = 1;
    float phi = position.y * PI;
    float theta = position.x * closing_increment* PI;
    
    vec3 pos;
    pos.x = rho * sin(phi) * cos(theta);
    pos.y = rho * sin(phi) * sin(theta);
    pos.z = rho * cos(phi);
    
    return pos;
}
  
// calculate model position
vec3 calculateModelPosition(vec2 position){
	vec3 afterFunctionPosition;
	if (functionSwitcher == 0) return funkce_k1_strecha(position);
	if (functionSwitcher == 1) return funkce_k2_trychtyr(position);
	if (functionSwitcher == 2) return funkce_c1_valec(position);
	if (functionSwitcher == 3) return funkce_c2_lava(position);
	if (functionSwitcher == 4) return funkce_s1_sfera(position);
	if (functionSwitcher == 5) return funkce_s2_closing_sfere(position);
	
	// defaults
	return funkce_k1_strecha(position); 
}

// calculate normal using differencies
vec3 calculateNormal(vec2 uv){
	vec3 normal;
	vec3 fceA;
	vec3 fceB;
	
	normal.z = 1;
	
	if (functionSwitcher == 0) {
		fceA = funkce_k1_strecha(uv-vec2(delta,0.0));
		fceB = funkce_k1_strecha(uv-vec2(0.0,delta));
		}
	if (functionSwitcher == 1){
		fceA = funkce_k2_trychtyr(uv-vec2(delta,0.0));
		fceB = funkce_k2_trychtyr(uv-vec2(0.0,delta));
		}
	if (functionSwitcher == 2){
		fceA = funkce_c1_valec(uv-vec2(delta,0.0));
		fceB = funkce_c1_valec(uv-vec2(0.0,delta));
		} 
	if (functionSwitcher == 3){
		fceA = funkce_c2_lava(uv-vec2(delta,0.0));
		fceB = funkce_c2_lava(uv-vec2(0.0,delta));
		} 
	if (functionSwitcher == 4){
		fceA = funkce_s1_sfera(uv-vec2(delta,0.0));
		fceB = funkce_s1_sfera(uv-vec2(0.0,delta));
		} 
	if (functionSwitcher == 5){
		fceA = funkce_s2_closing_sfere(uv-vec2(delta,0.0));
		fceB = funkce_s2_closing_sfere(uv-vec2(0.0,delta));
		} 
	
	normal.x = (fceA.z - fceB.z) /(2.0*delta);
	normal.y = (fceA.z - fceB.z) / (2.0*delta);
	
	return normal;
	
}



void main() {
	
	// modifikace pozice
	vec2 position = inPosition;
	/*position *= 20;
	position -= 0.5;*/
	
	normal = calculateNormal(position);

	vec3 afterFunctionPosition = calculateModelPosition(position);
		
	// směr svetla
	vec4 objectPosition = matView * vec4(afterFunctionPosition,1.0);
	vec3 lightDirection = lightPosition.xyz - objectPosition.xyz;
	
	float NDotL = max(dot(normalize(lightDirection),normalize(normal)),0.0); // cosinus uhlu mezi vektory
	perVertexColor = NDotL*Diffuse;
	
	gl_Position = matProjection * matView * vec4(afterFunctionPosition,1.0);
	 
} 