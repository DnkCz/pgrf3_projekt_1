#version 150

uniform sampler2D texture; // rgb textura
uniform sampler2D texturen; // normalova textura
uniform sampler2D textureh; // vyskova textura


in vec3 lightVector;
in vec3 viewVector;
in vec3 normal;

in vec2 texCoord;

in float distanceFromLight;

in vec3 normalV;
 

uniform vec4 Ambient;
uniform vec4 BaseColor;
uniform vec4 Diffuse;
uniform vec4 Specular;

const float SpecularPower = 0.3;


// koeficienty utlumu svetla
uniform float constantAttenuation = 0.90;
uniform float linearAttenuation = 0.01;
uniform float quadraticAttenuation =0.001;

uniform float spotCutOff;

// smer svetla 
vec3 spotDirection =vec3(1.0,1.0,-1.0);

uniform vec2 cBumpSize; 

void main() {

vec3 ld = normalize(lightVector);
vec3 vd = normalize(viewVector);

// ziskame vysku z textury
float height = texture2D(textureh,texCoord).r;
height = height * cBumpSize.x + cBumpSize.y;

vec2 textCoordAndHeigh = texCoord.rg + ld.xy * height;


vec4 colorFromTexture = texture2D(texture, textCoordAndHeigh);
vec3 normalFromTexture = texture2D(texturen, textCoordAndHeigh).xyz * 2 -1.0; // p�evod soustavy od 0 do 1 na rozsah od -1 do 1

normalFromTexture = normalize(normalFromTexture);

float NDotL = max(dot( normalFromTexture,ld ),0.0 );

float NDotHV = max(0.0,dot(normalFromTexture,normalize(ld+vd)));
vec4 totalSpecular = Specular *(pow (NDotHV,10.0)); 

vec4 totalAmbient = Ambient ;
vec4 totalDiffuse = Diffuse * NDotL;

gl_FragColor = colorFromTexture *(totalDiffuse + totalAmbient) + totalSpecular;


} 