#version 330

uniform sampler2D texture; // rgb textura
uniform sampler2D texturen; // normalova textura

uniform vec4 BaseColor;
uniform vec4 Ambient;
uniform vec4 Diffuse;
uniform vec4 Specular;

uniform float spotCutOff;
uniform float constantAttenuation;
uniform float linearAttenuation;
uniform float quadraticAttenuation;


in vec3 lightVector;
in vec3 viewVector;
in vec3 normal;

in vec2 texCoord;

in float distanceFromLight;

in vec3 normalV;
 
// asi chyba v prednaskach power bude spis float
const float SpecularPower = 0.3;


// smer svetla 
vec3 spotDirection =vec3(1.0,1.0,-1.0);
 
void main() {

vec4 colorFromTexture = texture2D(texture, texCoord);
vec3 normalFromTexture = texture2D(texturen, texCoord).xyz * 2 -1.0; // p�evod soustavy od 0 do 1 na rozsah od -1 do 1

vec3 ld = normalize(lightVector);
vec3 nd = normalize(normal);
vec3 vd = normalize(viewVector);

normalFromTexture = normalize(normalFromTexture);

float NDotL = max(dot( normalFromTexture,ld),0.0 );
float NDotHV = max(0.0,dot(normalFromTexture,normalize(ld+vd) ));
vec4 totalSpecular = Specular *(pow (NDotHV,10.0));

vec4 totalAmbient = Ambient ;
vec4 totalDiffuse = Diffuse * NDotL;

gl_FragColor = colorFromTexture *(totalDiffuse + totalAmbient) + totalSpecular;



} 