#version 150
// odpov�d� tutor3_light3
in vec2 inPosition; // input from the vertex buffer
in vec3 inColor; // input from the vertex buffer
out vec3 vertColor; // output from this shader to the next pipleline stage
uniform float time; // variable constant for all vertices in a single draw
uniform mat4 mat;
uniform mat4 matProjection;
uniform mat4 matView;


out vec3 lightVector;
out vec3 viewVector;
out vec3 normal;
out vec2 texCoord;

out vec4 positionInViewCoordinates;
out float distanceFromLight;

// pozice svetla .. zase z appky
const vec3 lightPosition = vec3(1.0,1.0,1.0);

float funkce(vec2 position) {
return cos(sqrt(position.x * position.x + position.y*position.y));
}

// calculated normal using differences 
vec3 normalDiff (vec2 uv){
float delta = 0.01;
vec3 n;
n.z = 1;
n.x = (funkce(uv-vec2(delta,0.0)) - (funkce(uv+vec2(delta,0.0)) )) /(2.0*delta);
n.y = (funkce(uv-vec2(0.0,delta)) - (funkce(uv+vec2(0.0,delta)) )) / (2.0*delta);

return n;
}


void main() {
	vec2 position = inPosition;
	// pozice do textury
	texCoord = inPosition;
	
	position -=0.5;
	position *=50;

	float z = funkce(position.xy);
	
	
	normal = normalDiff(position);

	positionInViewCoordinates = matView * vec4(position,z,1.0);
	lightVector = lightPosition - positionInViewCoordinates.xyz;
	
	distanceFromLight = length(lightVector); // pro vypocet utlumu
	
	viewVector = -positionInViewCoordinates.xyz;
	
	gl_Position = matProjection * matView * vec4(position,z,1.0); // rozd�len� matic
	
} 