#version 150
in vec4 perVertexColor; // input from the previous pipeline stage
out vec4 outColor; // output from the fragment shader

void main() {
outColor = perVertexColor; 
} 