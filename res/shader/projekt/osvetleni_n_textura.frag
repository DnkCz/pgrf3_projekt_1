#version 150

uniform sampler2D texture; // rgb textura
uniform sampler2D texturen; // normalova textura


in vec3 lightVector;
in vec3 viewVector;
in vec3 normal;

in vec2 texCoord;

in float distanceFromLight;

 
// todo poslat z appky
const vec4 Ambient = vec4(0.5,0.0,0.0,1.0);
const vec4 BaseColor = vec4(0.0,0.5,0.0,1.0);
const vec4 Diffuse = vec4(0.0,0.0,0.5,1.0);
const vec4 Specular = vec4(0.0,0.0,0.5,1.0);
// asi chyba v prednaskach power bude spis float
const float SpecularPower = 0.3;

// todo zase predat z appky
// koeficienty utlumu svetla
const float constantAttenuation = 0.90;
const float linearAttenuation = 0.01;
const float quadraticAttenuation =0.001;

// todo z appky
const float spotCutOff = 0.5; // cosinus 60ti stupnu = 0.5
 
// smer svetla 
vec3 spotDirection =vec3(1.0,1.0,-1.0);
 
void main() {

vec4 colorFromTexture = texture2D(texturen, texCoord);

vec3 ld = normalize(lightVector);
vec3 nd = normalize(normal);
vec3 vd = normalize(viewVector);

float NDotL = max(dot( normalize(lightVector),normalize(normal)),0.0 );

// odraz spocitan dle prednasky reflection = 2*(L . N) * N  - L
vec3 reflection = normalize(2*NDotL * (nd) -ld);
float RDotV = max(dot( normalize(reflection),normalize(vd)),0.0 );

// dle prednasky h = ( L+E )/ | L+E | .. ale chceme normalizovaný
vec3 halfVector = normalize((ld+vd)/length(ld+vd)); 


float NDotH = max(dot( normalize(normal),normalize(halfVector)),0.0 );


vec4 totalAmbient = Ambient + BaseColor;
vec4 totalDiffuse = Diffuse * NDotL *BaseColor;
vec4 totalSpecular = Specular *(pow (NDotH,SpecularPower * 4.0));

// vypocet utlumu
float attenuation = 1.0/(
			constantAttenuation 
			+ linearAttenuation * distanceFromLight 
			+ quadraticAttenuation*distanceFromLight*distanceFromLight
			);
			

float spotEffect = dot(normalize(spotDirection),normalize(-ld));
if (spotEffect > spotCutOff){
	gl_FragColor = totalAmbient*colorFromTexture + attenuation*(totalDiffuse*colorFromTexture + totalSpecular);
} else {
	//gl_FragColor = vec4(0.0,0.0,1.0,1.0);
	//	gl_FragColor = vec4(vec3(spotEffect),1.0);
	gl_FragColor = totalAmbient;  
}


} 