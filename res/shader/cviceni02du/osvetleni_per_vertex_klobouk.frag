#version 150
in vec4 color; // input from the previous pipeline stage
out vec4 outColor; // output from the fragment shader
void main() {
	outColor = color; 
} 