#version 150
in vec2 inPosition; // input from the vertex buffer
in vec3 inColor; // input from the vertex buffer
out vec4 color; // output from this shader to the next pipleline stage
uniform float time; // variable constant for all vertices in a single draw
uniform mat4 mat; // view * projection matice

uniform mat4 matView; // view matice - přidáno pro per vertex osvětlení

// funkce vykreslující "klobouk"
float klobouk_funkce(vec2 position){
return cos(sqrt(position.x * position.x + position.y*position.y));
}

// funkce na výpočet normály na základě diferencí v okolních bodech posunutých o delta
vec3 normalDiff (vec2 uv){
float delta = 0.01;
vec3 dzdu = vec3(1.0,0.0,(klobouk_funkce(uv+vec2(delta,0))-klobouk_funkce(uv-vec2(delta,0)))/2.0/delta);
vec3 dzdv = vec3(0.0,1.0,(klobouk_funkce(uv+vec2(0,delta))-klobouk_funkce(uv-vec2(0,delta)))/2.0/delta);
return cross(dzdu,dzdv);
}




void main() {
	vec2 position = inPosition;
	//position.x += 0.1;
	//position.y += cos(position.x + time);
	
	// modifikace pozice.. zvětšení posunutí 
	position -=0.5;
	position *=2;
	position *=10;
	
	// zavolání funkce na vrácení Z-tové souřadnice
	float z = klobouk_funkce(position);
	
	// výpočet normály
	vec3 normal = normalDiff(position.xy);
	
	// zvolíme konstantní pozici světla
	vec4 lightPosition = vec4(-10.0,5.0,2.0,1.0);
	
	// difuzní složka
	vec4 diffuse = vec4(1.0,1.0,1.0,1.0);
	
	// pozice objektu
	vec4 objectPosition = matView * vec4(position.x,position.y,z,1.0); 
	
	// směr světla
	vec3 lightDirection = lightPosition.xyz - objectPosition.xyz;
	
	// dot product - skalární součin normáli a vektoru ke světlu.. bereme jen kladné hodnoty, proto max
	float NdotL = max(dot(normalize(lightDirection),normalize(normal)),0.0);
	
	// výstupní barva per vertex barva pro FS
	color = NdotL*diffuse;

		//  matice pohledová a projekční by tom měla být a zároveň v gl_Position je objectPosition
	gl_Position = mat * vec4(position,z,1.0);
	
}

 