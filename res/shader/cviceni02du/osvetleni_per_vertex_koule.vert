#version 150
in vec2 inPosition; // input from the vertex buffer
in vec3 inColor; // input from the vertex buffer
out vec4 color; // output from this shader to the next pipleline stage
uniform float time; // variable constant for all vertices in a single draw
uniform mat4 mat; // view * projection matice

uniform mat4 matView; // view matice - pÅ™idÃ¡no pro per vertex osvÄ›tlenÃ­

const float PI=3.1415926;

// fce vykreslující kouli
vec3 koule (vec2 uv){
float azimuth = uv.x*2.0*PI;
float zenith = uv.y*PI;
vec3 position;

position.x=cos(azimuth)*cos(zenith);
position.y=2.0*sin(azimuth)*cos(zenith);
position.z=3.0*sin(zenith);
return position;
}


vec3 normalDiff (vec2 uv){
float delta = 0.01;
vec3 dzdu = vec3(1.0,0.0,(koule(uv+vec2(delta,0))-koule(uv-vec2(delta,0)))/2.0/delta);
vec3 dzdv = vec3(0.0,1.0,(koule(uv+vec2(0,delta))-koule(uv-vec2(0,delta)))/2.0/delta);
return cross(dzdu,dzdv);
}




void main() {
	vec2 position = inPosition;
	
	// modifikace pozice.. zvětšení posunutí	
/*  position -=0.5;
	position *=2;
	position *=10;*/
	
	// zavolání funkce na vrácení Z-tové souřadnice	
	vec3 position_koule = koule(position);
	float z = position_koule.z;
	
	
	// výpočet normály	
	vec3 normal = normalDiff(position.xy);
	
	// zvolíme konstantní pozici světla
	vec4 lightPosition = vec4(-10.0,5.0,2.0,1.0);
	
	// difuzní složka
	vec4 diffuse = vec4(1.0,1.0,1.0,1.0);
	
	// pozice objektu
	vec4 objectPosition = matView * vec4(position_koule.x,position_koule.y,z,1.0); 
	
	// směr světla
	vec3 lightDirection = lightPosition.xyz - objectPosition.xyz;
	
	// dot product - skalární součin normáli a vektoru ke světlu.. bereme jen kladné hodnoty, proto max	
	float NdotL = max(dot(normalize(lightDirection),normalize(normal)),0.0);
	
	// výstupní barva per vertex barva pro FS
	color = NdotL*diffuse;

	//  matice pohledová a projekční by tom měla být a zároveň v gl_Position je objectPosition		
	gl_Position = mat * vec4(position_koule.xy,z,1.0);
	
}

 