#version 150
in vec3 vertColor; // input from the previous pipeline stage
out vec4 outColor; // output from the fragment shader

in vec3 lightVector;
in vec3 viewVector;
in vec3 normal;
in vec2 texCoord;
uniform sampler2D texture;
uniform sampler2D texturen;

in vec4 positionInViewCoordinates;
in vec3 normalV;
 
void main() {

outColor = vec4(texCoord,0.0,1.0);
outColor = texture2D(texturen, texCoord);

//
vec3 normal_from_texture = texture2D(texturen, texCoord*vec2(2.0,0.5)).xyz * 2 -1.0; // p�evod soustavy od 0 do 1 na rozsah od -1 do 1

float diffuse = max(dot(normalize(lightVector),normalize(normal_from_texture)),0.0);
outColor = vec4(normalize(normal_from_texture),1.0);
outColor = vec4(vec3(diffuse),1.0);
//outColor = vec4(normalize(normal),1.0);
//outColor = vec4(vec(difuse),1.0);

} 